﻿using System;
using IntegersAdderLib;

namespace ManulTests
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var adder=new IntegersAdder();

            var numbers = adder.ReadFile(args[0]);

            var result = adder.Count(numbers);

            Console.WriteLine(result);
            Console.ReadLine();
        }
    }
}