﻿using IntegersAdderLib;
using Moq;
using NUnit.Framework;

namespace ItegersAdderTests
{
    [TestFixture]
    public class IntegersAdderShould
    {
        [Test]
        public void ReturnsFileContene_IfFile_IsExists()
        {
            string[] expectedResult = {"1", "2", "3"};
            var adder = Mock.Of<IIntegersAdder>(x => x.ReadFile(@"D:\testFile.txt") == expectedResult);
            var stub = new StubIntegersAdder(adder);

            var result = stub.Adder.ReadFile(@"D:\testFile.txt");

            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void ReturnsSumOfIntegers_IfInput_IsArrayOfStrings()
        {
            string[] numbers = {"1", "2", "3"};

            var adder = new IntegersAdder();
            var result = adder.Count(numbers);
            Assert.AreEqual(1 + 2 + 3, result);
        }

        [Test]
        public void ReturnsTrue_IfFile_IsExists()
        {
            var adder = Mock.Of<IIntegersAdder>(x => x.IsExists(@"D:\testFile.txt"));
            var stub = new StubIntegersAdder(adder);

            var isExists = stub.Adder.IsExists(@"D:\testFile.txt");

            Assert.IsTrue(isExists);
        }
    }
}