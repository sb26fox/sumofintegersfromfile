﻿using System.IO;

namespace IntegersAdderLib
{
    public interface IIntegersAdder
    {
        string[] ReadFile(string path);
        bool IsExists(string path);
        int Count(string[] numbers);
    }

    public class IntegersAdder : IIntegersAdder
    {
        public bool IsExists(string path)
        {
            return File.Exists(path);
        }

        public int Count(string[] numbers)
        {
            var result = 0;

            foreach (var number in numbers)
            {
                if (!string.IsNullOrEmpty(number))
                {
                    result += int.Parse(number);
                }
            }

            return result;
        }

        public string[] ReadFile(string path)
        {
            if (IsExists(path))
            {
                return File.ReadAllLines(path);
            }
            else
            {
                throw new FileNotFoundException();
            }
        }
    }

    public class StubIntegersAdder
    {
        public IIntegersAdder Adder;

        public StubIntegersAdder(IIntegersAdder adder)
        {
            Adder = adder;
        }

        public StubIntegersAdder()
        {
            Adder = new IntegersAdder();
        }
    }
}